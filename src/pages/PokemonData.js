import React, { useEffect, useState } from "react";
import DataTable from "../components/DataTable";
import Layout from "../components/shared/Layout";

const PokemonData = (props) => {
  const id = props.match.params.id;

  const [pokemon, setPokemon] = useState({});
  const [dataLoaded, setLoaded] = useState(false);

  useEffect(() => {
    fetch("https://pokeapi.co/api/v2/pokemon/"+id)
      .then((response) => response.json())
      .then((data) => {
        setPokemon(data);
        setLoaded(true);
      });
  }, []);
  
  const formatData = (data, prop) => {
    return data?.map((d) => d[prop].name);
  };

  return (
    <Layout pageType="page-pokemon-data">
      {!dataLoaded ? (
        "loading"
      ) : (
        <>
          <section>
            <section>
              <img
                alt="pokemon"
                src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`}
              />
            </section>

            <section>
              <DataTable
                title="Abilities"
                data={formatData(pokemon.abilities, "ability")}
              />
              <DataTable
                title="Types"
                data={formatData(pokemon.types, "type")}
              />
            </section>
          </section>

          <section></section>
        </>
      )}
    </Layout>
  );
};

export default PokemonData;