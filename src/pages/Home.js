import React, { useEffect, useState } from "react";
import PokeList from "../components/PokeList";
import Layout from "../components/shared/Layout";

const Home = () => {
  const [pokemons, setPokemons] = useState([]);

  useEffect(() => {
    fetch("https://pokeapi.co/api/v2/pokemon")
      .then((response) => response.json())
      .then((data) => setPokemons(data.results));
  }, []);

  return (
    <Layout pageType="page-home">
      <PokeList list={pokemons} />
    </Layout>
  );
};

export default Home;