import React from "react";

const DataTable = ({ title, data }) => {
  return (
    <div className="c-datatable">
      <h3>{title}</h3>
      <div></div>
      <ul>
        {data.map((text) => (
          <li>{text}</li>
        ))}
      </ul>
    </div>
  );
};

export default DataTable;