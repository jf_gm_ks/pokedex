import React from "react";
import PokeItem from "./PokeItem";
import { CustomList } from "./styled/styles";

const PokeList = ({ list }) => {
  return (
    <CustomList className="c-pokelist">
      {list.map((pokemon, idx) => {
        return (
          <PokeItem key={`pokeitem-${idx}`} name={pokemon.name} id={idx + 1} />
        );
      })}
    </CustomList>
  );
};

export default PokeList;
