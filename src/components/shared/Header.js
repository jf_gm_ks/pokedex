import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { CustomHeader, HeaderList } from "./../styled/styles";

const Header = ({ headerType }) => {
  return (
    <CustomHeader>
      <HeaderList>
        <li>
          <Link to="/">Home</Link>
        </li>
      </HeaderList>
    </CustomHeader>
  );
};

Header.propTypes = {
  headerType: PropTypes.string.isRequired
};

export default Header;