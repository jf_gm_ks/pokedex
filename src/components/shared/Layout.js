import React from "react";
import Header from "./Header";
import { CustomLayout } from "./../styled/styles";

const Layout = ({ children }) => {
  return (
    <CustomLayout>
      <Header headerType="principal" />

      {children}
    </CustomLayout>
  );
};

export default Layout;