import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

const PokeItem = ({ name, id }) => {
  const history = useHistory();

  const goToPokemon = () => {
    history.push(`/pokemon/${id}`);
  };

  return (
    <div className="c-pokeitem">
      <div onClick={goToPokemon}>
        <img
          src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`}
        />
      </div>
      <div>
        {name} {id}
      </div>
    </div>
  );
};

export default PokeItem;
