import styled from "styled-components";

export const CustomLayout = styled.div`
  padding: 15px;
  padding-top: 50px;
  position: absolute;
`;

export const CustomHeader = styled.header`
  position: fixed;
  top: 0;
  left: 0;
  background-color: #801336;
  width: 100%;
`;

export const HeaderList = styled.ul`
  display: flex;
  margin: 10px 10px;
`;

export const CustomList = styled.div`
  display: flex;
  justify-content: center;
  flex-flow: row wrap;
`;

export const imageContainer = styled.image``;