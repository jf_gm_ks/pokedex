import { createStore } from 'redux';

/** Nombres de constantes de acciones */
const INCREMENT = 'INCREMENT';
const DECREMENT = 'DECREMENT'

const initialState = {
    counter: 0,
    power:'off'
};
/** Reduce
 * Función pura que retorna el estado actual
 */
function reducer(state = initialState, action) {
    
    switch(action.type) {
        case INCREMENT:
            return {
                ...state,
                counter: state.counter + 1
            }

        case DECREMENT:
            return {
                ...state,
                counter: state.counter - 1
            }

        default:
            return state
    }

}

/** Store
 * Almacenamiento de nuestro estado
 */
const store = createStore(reducer)

/** Creadores de acciones */
const increment = {
    type: INCREMENT
}

const decrement = {
    type: DECREMENT
}

// store.getState()
// store.dispatch()
// store.subscribe(f)

store.subscribe(() => console.log(store.getState()))

store.dispatch(increment)
store.dispatch(decrement)


setTimeout(() => {
    store.dispatch(increment)
}, 2000)

export default store