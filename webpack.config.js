const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')

const jsRules = {
    test: /\.js$/,
    exclude: /node_modules/,
    use: {
        loader: 'babel-loader',
        options: {
            presets: [
                '@babel/preset-react',
                '@babel/preset-env'
            ]
        }
    }
}

const cssRules = {
    test: /\.(css|scss)$/,
    use: ["style-loader", "css-loader"],
}

module.exports = {
    entry: './src/index.js',
    output: {
        filename: "app.[contentHash].js",
        path: path.resolve(__dirname,'dist'),
        publicPath: '/'
    },
    module: {
        rules: [jsRules, cssRules]
    },
    devServer: {
        historyApiFallback: true,
        port: 3000
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/public/index.html'
        })
    ]
}